function get_data_event() {
    let url = "https://gist.githubusercontent.com/JEnriquePS/158a243319d342fb2a8fa822880f6b84/raw/4e3f48fe4cc4027fd0d5c59683c87eff9c9b6f94/schedule.json"
    return fetch(url
    ).then(response => {
        return response.json();
    })
}

function success_dates_event() {
    return Promise.all([get_data_event()]);
}

function get_data_time_session(session){
    let start_hour = session.starting_slot.split(":")[0]
    let start_minutes = session.starting_slot.split(":")[1].split(' ')[0]
    let end_hour =  session.ending_slot.split(":")[0]
    let end_minutes = session.ending_slot.split(":")[1].split(' ')[0]
    return  { start: {hour: parseInt(start_hour), minute: parseInt(start_minutes)}, end: {hour: parseInt(end_hour), minute: parseInt(end_minutes)}}
}

function get_rest_for_height_card_schedule(data) {
    let rest = 0;
    if (data.start.hour === data.end.hour) {
        rest = parseInt(data.end.minute) - parseInt(data.start.minute)
        return rest
    } else if (data.start.hour < data.end.hour) {
        let before_minutes = ((parseInt(data.end.hour) - parseInt(data.start.hour)) * 60)
        let exact_minutes = before_minutes - parseInt(data.start.minute)
        return parseInt(data.end.minute) + exact_minutes
    }
}

function get_height_px_card(data) {
    return (get_rest_for_height_card_schedule(data) * 100) / 60
}

function get_width_card(session, c_index_columns, c_columns){
    let c_column = c_columns[c_index_columns]

    if (c_columns.length - 1 > c_index_columns){
        console.log(c_columns[c_index_columns + 1].sessions.length)
        for (let i = c_index_columns + 1; i <= c_columns.length - 1; i++) {
            let next_column = c_columns[c_index_columns + 1]
        }
    }
    return 0
}

function get_top_card(group, c_index_group, list_columns, c_index_column, c_index_session){

    let current_session;
    let before_session;
    let time_end_before_session;
    let time_start_current_session;
    console.log(list_columns[c_index_column].sessions[c_index_session])
    console.log(c_index_column)
    console.log(c_index_session)
    if (c_index_column === 0 && c_index_session === 0){
        return 0
    }
    if (c_index_session > 0){
        before_session = get_data_time_session(list_columns[c_index_column].sessions[c_index_session - 1])
        current_session = get_data_time_session(list_columns[c_index_column].sessions[c_index_session])

        if (before_session.end.hour === current_session.start.hour && before_session.end.minute === before_session.start.minute){
            return 0
        }
        if (before_session.end.hour === current_session.start.hour && before_session.end.minute <= current_session.start.minute){
            time_end_before_session = [before_session.end.hour, before_session.end.minute]
            time_start_current_session = [current_session.start.hour, current_session.start.minute]
        }
        else{
            time_end_before_session = [before_session.start.hour, before_session.start.minute]
            time_start_current_session = [current_session.start.hour, current_session.start.minute]
        }

    }
    if(c_index_column > 0 && c_index_session === 0){
        before_session = get_data_time_session(group[c_index_group].summary)
        current_session = get_data_time_session(list_columns[c_index_column].sessions[c_index_session])
        time_end_before_session = [before_session.start.hour, before_session.start.minute]
        time_start_current_session = [current_session.start.hour, current_session.start.minute]
    }
    /*if (l>0){
        before_session = data.groups[l-1].sessions[data.groups[l-1].sessions.length-1]
        time_end_before_session = [before_session.end.hour, before_session.end.minute]
        time_start_current_session = [group.sessions[k].start.hour, group.sessions[k].start.minute]
    }*/
    let now  = `${time_end_before_session[0]}:${time_end_before_session[1]}`
    let before = `${time_start_current_session[0]}:${time_start_current_session[1]}`;
    let blank_minutes = moment(before,"HH:mm").diff(moment(now,"HH:mm"), 'minutes')
    return (blank_minutes * 200)/60
}

function get_top_group(group,i){
    if (i===0){
        return 0
    }
    let before_group = group[i-1]
    let current_group = group[i]
    let time_end_before_session;
    let time_start_current_session;
    let current_session = get_data_time_session(current_group.summary)
    let before_session;
    if (i>0){
        before_session = get_data_time_session(before_group.summary)
        time_end_before_session = [before_session.end.hour, before_session.end.minute]
        time_start_current_session = [current_session.start.hour, current_session.start.minute]
    }

    let now  = `${time_end_before_session[0]}:${time_end_before_session[1]}`
    let before = `${time_start_current_session[0]}:${time_start_current_session[1]}`;
    let blank_minutes = moment(before,"HH:mm").diff(moment(now,"HH:mm"), 'minutes')
    return Math.abs((blank_minutes * 200)/60)

}

function get_height_group(group,i){


    let current_group = group[i]
    let time_end_before_session;
    let time_start_current_session;
    let current_session = get_data_time_session(current_group.summary)

    before_session = get_data_time_session(current_group.summary)
    time_end_before_session = [before_session.end.hour, before_session.end.minute]
    time_start_current_session = [current_session.start.hour, current_session.start.minute]

    let now  = `${time_end_before_session[0]}:${time_end_before_session[1]}`
    let before = `${time_start_current_session[0]}:${time_start_current_session[1]}`;
    let blank_minutes = moment(before,"HH:mm").diff(moment(now,"HH:mm"), 'minutes')
    return Math.abs((blank_minutes * 200)/60)
}

function get_card_schedule(group, c_index_group, c_index_column, c_index_session) {
    let session = group[c_index_group].columns[c_index_column].sessions[c_index_session]
    let list_columns = group[c_index_group].columns

    let session_height = get_height_px_card(get_data_time_session(session)) * 2.0
    let session_width = get_width_card(session, c_index_column, list_columns)
    let session_top = get_top_card(group, c_index_group, list_columns, c_index_column, c_index_session);
    return `
            <div class="schedule-item disabled" style="height: ${session_height}px; margin-top:${session_top}px; position: relative">
                <div class="schedule-item--line">
                    <div class="schedule-item--title">
                        <h3 class="title g-cut">${session.title}</h3>
                         
                    </div>
                    <p class="d-none d-md-block">${session.starting_slot} - ${session.ending_slot}</p>
                    <p><span></span></p>
                </div>
            </div>
            `
}


function get_schedule_body(data) {
    let block_schedule = '';
    let group = data;
    block_schedule += `<div class="schedule--body__day content_day" id="body_date" style="display: block">`;
    for (let i = 0; i < group.length; i++) {
        if (i !== 0) {
            //if (group[i - 1].start_of_hour.slot === group[i].start_of_hour.slot) {
                //TODO
            //}
        }
        console.log(group[i])
        block_schedule += `<div class="schedule-box-1" data-id_group="${group[i].name}" style="height: ${get_height_group(group,i)}px; display: flex; margin-top:${get_top_group(group,i)}px; margin-bottom: 5px">`
        for (let j = 0; j < group[i].columns.length; j++) {
            let width_column = 100/group[i].columns.length;
            block_schedule += `<div style="display: flex; flex-wrap: wrap; width: ${width_column}%; margin-right: 5px">`
            for (let k = 0; k < group[i].columns[j].sessions.length; k++){
                let c_group = group[i]
                let c_columns = group[i].columns
                let c_session = group[i].columns[j].sessions[k]

                block_schedule += get_card_schedule(group, i, j, k) // card
            }
            block_schedule += `</div>`;
        }
        block_schedule += `</div>`;
    }
    block_schedule += `</div>`;
    document.getElementById('schedule--body__container').innerHTML = block_schedule
}


success_dates_event().then(([data]) => {
    get_schedule_body(data)
})
